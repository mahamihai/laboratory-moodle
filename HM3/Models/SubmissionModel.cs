﻿namespace HM3.Models
{
    public class SubmissionApi
    {
        public int Id { get; set; }
        public int Assign_Id { get; set; }
        public int Student_Id { get; set; }
        public string GitLink { get; set; }
        public string Note { get; set; }
        public double Grade { get; set; }
    }
}