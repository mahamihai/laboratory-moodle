﻿using System;

namespace HM3.Models
{
    public class LaboratoryAPI
    {
        public string Title { get; set; }
        public string Curricula { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<int> Number { get; set; }
        public string Description { get; set; }
    }
}