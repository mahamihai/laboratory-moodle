﻿namespace HM3.Models
{
    public class AttendanceModel
    {
        public int Id { get; set; }
        public int LabId { get; set; }
        public int StudentId { get; set; }
    }
}