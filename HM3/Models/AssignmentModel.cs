﻿using System;

namespace HM3.Models
{
    public class AssignmentModel
    {

        public int Id { get; set; }
        public Nullable<int> Lab_Id { get; set; }
        public string Name { get; set; }
        public Nullable<System.DateTime> Deadline { get; set; }
        public string Description { get; set; }
    }
}