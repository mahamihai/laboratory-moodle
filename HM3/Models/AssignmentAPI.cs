﻿using System;

namespace HM3.Models
{
    public class AssignmentAPI
    {



        public Nullable<int> Lab_Id { get; set; }
        public string Name { get; set; }
        public Nullable<System.DateTime> Deadline { get; set; }
        public string Description { get; set; }
    }
}