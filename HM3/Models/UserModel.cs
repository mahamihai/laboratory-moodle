﻿namespace HM3.Models
{
    public class UserModel
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public string GroupNr { get; set; }
        public string FullName { get; set; }
        public string Hobby { get; set; }
    }
}