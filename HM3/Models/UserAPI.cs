﻿namespace HM3.Models
{
    public class UserAPI
    {
        public string Type { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public string GroupNr { get; set; }
        public string FullName { get; set; }
        public string Hobby { get; set; }
    }
}