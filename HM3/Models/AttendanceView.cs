﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HM3.Models
{
  public  class AttendanceView
    {
        public int Id { get; set; }
        public int LabId { get; set; }
        public string Name { get; set; }
    }
}
