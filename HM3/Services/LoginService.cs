﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HM3.Models;
using HM3.Views;
using RestSharp;
using RestSharp.Authenticators;

namespace HM3.Services
{
    public class LoginService
    {
        private RestClient _client;

        public LoginService()
        {
            this._client=new RestClient("http://localhost:52513");
        }
        public UserModel checkUser(string username,string password)
        {
            RestRequest request=new RestRequest("Users/login",Method.GET);
            request.AddParameter("uss", username);
            request.AddParameter("pass", password);
            var response = _client.Execute<UserModel>(request).Data;
            if (response != null)
            {
                switch (response.Type)
                {


                    case "Admin":
                        _client.Authenticator = new HttpBasicAuthenticator(username, password);
                        var TeacherServices=new TeacherService(this._client);
                            TeacherVIew tView=new TeacherVIew(TeacherServices);
                            tView.Show();

                        break;
                    case "Student":
                        _client.Authenticator = new HttpBasicAuthenticator(username, password);
                        var StudentService=new StudentService(this._client);
                        StudentForm sForm=new StudentForm(StudentService);
                        sForm.Show();

                        break;
                     default: MessageBox.Show("Kinda sucks");
                        break;
                }
            }
            else
            {
                MessageBox.Show("Something was not good");
            }

            return response;

        }


        public void Register(string token, string email, string password)
        {
            RestRequest request = new RestRequest("Users/{token}/{name}/{pass}", Method.PUT);
            request.AddQueryParameter("token", token);
            request.AddQueryParameter("name", email);
            request.AddQueryParameter("pass",password);
          var t= _client.Execute(request).StatusCode;
           
        }
    }
}
