﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HM3.Models;
using RestSharp;

namespace HM3.Services
{
    public class StudentService
    {
        private RestClient _client;
        public StudentService(RestClient client)
        {
            this._client = client;

        }

        public List<LaboratoryModel> getLabs()
        {
            var request = new RestRequest("Laboratory/", Method.GET);
            var response = _client.Execute<List<LaboratoryModel>>(request).Data;
            return response;
        }

        public List<AssignmentModel> getLabAssignments(int labId)
        {
            var request = new RestRequest("Assignments/{labId}", Method.GET);
            request.AddParameter("labId", labId);
            var response = _client.Execute<List<AssignmentModel>>(request).Data;
            return response;
        }

        public void makeSubmission(string git, string remark)
        {
        
        }

    }
}
