﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HM3.Services;

namespace HM3.Views
{
    public partial class Register : Form
    {
        private LoginService _login;
        public Register(LoginService login)
        {
            InitializeComponent();
            this._login = login;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string token = this.textBox1.Text;
            string name = this.textBox2.Text;
            string password = this.textBox3.Text;

            this._login.Register(token, name, password);

        }
    }
}
