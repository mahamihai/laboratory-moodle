﻿using System;
using System.Windows.Forms;
using HM3.Services;

namespace HM3.Views
{
    public partial class StudentForm : Form
    {
        private StudentService _studentService;
        public StudentForm(StudentService sService)
        {
            InitializeComponent();
            _studentService = sService;
        }
        private int selectedLabId;
        private void gridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int id;


            try
            {
                int row = e.RowIndex;
                string value = this.dataGridView1.Rows[row].Cells["id"].Value.ToString();
                id = int.Parse(value);
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                return;
            }

            this.selectedLabId = id;
        }
        private void button1_Click(object sender, System.EventArgs e)
        {
            var labs = this._studentService.getLabs();
            this.dataGridView1.DataSource = labs;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var assignments = this._studentService.getLabAssignments(this.selectedLabId);
            this.dataGridView2.DataSource = assignments;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string remark = this.textBox2.Text;
            string git = this.textBox1.Text;

        }
    }
}
