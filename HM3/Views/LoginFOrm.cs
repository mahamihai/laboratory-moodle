﻿using System.Windows.Forms;
using HM3.Services;

namespace HM3.Views
{
    public partial class LoginForm : Form
    {
        private LoginService _login;
        public LoginForm()
        {
            InitializeComponent();
            this._login=new LoginService();
        }

        private void button1_Click(object sender, System.EventArgs e)
        {
            string email = this.textBox1.Text;
            string password = this.textBox2.Text;
            var response = this._login.checkUser(email, password);


        }

        private void button2_Click(object sender, System.EventArgs e)
        {
            Register regCassete=new Register(this._login);
            regCassete.Show();
        }
    }
}
