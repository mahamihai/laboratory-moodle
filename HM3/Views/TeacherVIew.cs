﻿using System;
using System.Windows.Forms;
using HM3.Models;
using HM3.Services;
using RestSharp;

namespace HM3.Views
{
    public partial class TeacherVIew : Form
    {
        private TeacherService _teacher;
        private int selectedLabId;
        private void gridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int id;


            try
            {
                int row = e.RowIndex;
                string value = this.dataGridView1.Rows[row].Cells["id"].Value.ToString();
                id = int.Parse(value);
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                return;
            }

            this.selectedLabId = id;
        }
        private int selectedStudentId;
        private void gridView_CellClick1(object sender, DataGridViewCellEventArgs e)
        {
            int id;


            try
            {
                int row = e.RowIndex;
                string value = this.dataGridView2.Rows[row].Cells["id"].Value.ToString();
                id = int.Parse(value);
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                return;
            }

            this.selectedStudentId = id;
        }
        private int selectedAssignmentId;
        private void gridView_CellClick2(object sender, DataGridViewCellEventArgs e)
        {
            int id;


            try
            {
                int row = e.RowIndex;
                string value = this.dataGridView3.Rows[row].Cells["id"].Value.ToString();
                id = int.Parse(value);
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                return;
            }

            this.selectedAssignmentId = id;
        }
        public TeacherVIew(TeacherService teacher)
        {
            InitializeComponent();
            this._teacher = teacher;
        
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var allLabs = this._teacher.getLabs();
            this.dataGridView1.DataSource = allLabs;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this._teacher.deleteLab(this.selectedLabId);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            LaboratoryAPI newLab = new LaboratoryAPI
            {
                Title = this.textBox1.Text,
                Curricula = this.textBox2.Text,
                Date = this.dateTimePicker1.Value,
                Number = Int32.Parse(this.textBox3.Text),
                Description = this.textBox4.Text
            };
            this._teacher.createLab(newLab);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            LaboratoryModel newLab = new LaboratoryModel
            {
                Id = this.selectedLabId,
                Title = this.textBox1.Text,
                Curricula = this.textBox2.Text,
                Date = this.dateTimePicker1.Value,
                Number = Int32.Parse(this.textBox3.Text),
                Description = this.textBox4.Text
            };
            this._teacher.updateLab(newLab);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            var students = this._teacher.getStudents();
            this.dataGridView2.DataSource = students;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this._teacher.DeleteStudent(this.selectedStudentId);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            string email = this.textBox5.Text;
            string group = this.textBox6.Text;
            string hobby = this.textBox7.Text;
            this._teacher.createStudent(email, group, hobby);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            UserModel newUser = new UserModel
            {
                Type = "Student",
                Id = this.selectedStudentId,
                Email = this.textBox5.Text,
                GroupNr = this.textBox6.Text,
                Hobby = this.textBox7.Text,
            };
            this._teacher.UpdateStudent(newUser);
        }

        private void button12_Click(object sender, EventArgs e)
        {
            var allAssi = this._teacher.getAssignmentsForLab(this.selectedLabId);
            this.dataGridView3.DataSource = allAssi;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            this._teacher.DeleteAssignment(this.selectedAssignmentId);

        }

        private void button10_Click(object sender, EventArgs e)
        {
            AssignmentAPI newAssi = new AssignmentAPI
            {
                Lab_Id = this.selectedLabId,
                Name = this.textBox8.Text,
                Deadline = this.dateTimePicker2.Value,
                Description = this.textBox10.Text
            };
            this._teacher.CreateAssignment(newAssi);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            AssignmentModel newAssi = new AssignmentModel
            {
                Id = this.selectedAssignmentId,
                Lab_Id = this.selectedLabId,
                Name = this.textBox8.Text,
                Deadline = this.dateTimePicker2.Value,
                Description = this.textBox10.Text
            };
            this._teacher.UpdateAssignemnts(newAssi);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            AttendanceAPI newAtt = new AttendanceAPI
            {
                LabId = this.selectedLabId,
                StudentId = this.selectedStudentId
            };
            this._teacher.MarkPresent(newAtt);
        }

        private void button14_Click(object sender, EventArgs e)
        {
            var labAtt = this._teacher.getAtenndanceForLab(this.selectedLabId);
            this.dataGridView4.DataSource = labAtt;
        }
    }
}
